export var isNull = function (term) {
    return term === null;
};
export var isNumber = function (term) {
    return typeof term === 'number';
};
export var isString = function (term) {
    return typeof term === 'string';
};
export var isObject = function (term) {
    return (!isNull(term) &&
        term instanceof Object &&
        term.constructor === Object);
};
export function objectHasProp(target, property) {
    // The `in` operator throws a `TypeError` for non-object values.
    return isObject(target) && property in target;
}
// eslint-disable-next-line @typescript-eslint/ban-types
export var isFunction = function (term) {
    return typeof term === 'function';
};

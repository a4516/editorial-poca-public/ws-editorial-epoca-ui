<!DOCTYPE html>
<html lang="es">

<head>
    <title>Editorial Época | EDESA 50 años</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
    <meta name="theme-color" content="#ef8e47">
    <meta name="description" content="Somos una empresa 100 % mexicana con cobertura nacional y en el extranjero, 
    la cual cuenta con más de 53 años en el mercado editorial y con más de 1300 títulos publicados en diferentes 
    colecciones de poesía, teatro, clásicos universales, entre otros." />
    <meta name="keywords" content="editorial, Editorial Época, Libros, Novelas, Nuevo Talento, 
    Apuntes Escolares, Clásicos Infantiles, HORUS, Divíertete y Aprende">
    <meta name="robots" content="index,follow" />
    <meta name="author" content="Editorial Época" />
    <meta name="copyright" content="Editorial Época" />
    <meta http-equiv="cache-control" content="max-age=31536000" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="canonical" href="https://www.editorialepoca.mx/" />
    <link rel="shortcut icon" href="<?php uri("main") ?>/flaticon.ico">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php uri("main") ?>/touch-icon-iphone.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="<?php uri("main") ?>/touch-icon-ipad.png" />
    <?php wp_head(); ?>
</head>
<header>

    <body>
        <div class="menu-elements">
            <div class="c-main-logo">
                <a href="<?php echo home_url(); ?>" aria-label="Pagina de inicio">
                </a>
            </div>
            <div class="c-menu-icon">
                <div class="_layer -top"></div>
                <div class="_layer -mid"></div>
                <div class="_layer -bottom"></div>
            </div>
        </div>

        <nav class="menuppal">
            <?php wp_nav_menu(array('theme_location' => 'header-menu')); ?>
        </nav>
        <section class="c-search-field">
            <?php echo get_search_form(); ?>
        </section>
</header>
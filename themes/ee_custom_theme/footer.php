<?php wp_footer(); ?>

<footer>
    <div class="c-footer">
        <div class="c-information">
            <div class="c-about-us-short">
                <div class="c-about-us-short-img">
                </div>
                <p>Somos una empresa 100 % mexicana con cobertura nacional y en el extranjero, la cual cuenta con más de
                    53 años en el mercado editorial y con más de 1300 títulos publicados en diferentes colecciones de
                    poesía, teatro, clásicos universales, entre otros. Nos adaptamos a necesidades y gustos por lo que
                    contamos con una variedad de ediciones como RTM, Diviértete y Aprende, Apuntes Escolares, Nuevo
                    talento, Clásicos infantiles y Horus. Estamos en constante evolución y siempre tenemos títulos en el
                    mercado que representen la actualidad en el mundo editorial.
                </p>
            </div>
            <div class="c-list-footer">
                <p>Nosotros</p>
                <?php wp_nav_menu(['theme_location' => 'footer-menu-nosotros']); ?>
            </div>
            <div class="c-contact">
                <p>Contáctanos</p>
                <div class="container-items-contacto">
                    <div class="item-contact"><a rel="noopener noreferrer"
                            href="https://www.facebook.com/EditorialEpocaMx" target="_blank" aria-label="Facebook"><i
                                class="fab fa-facebook-f"></i></a></div>
                    <div class="item-contact"><a href="mailto:pedidos@editorialepoca.mx" aria-label="Envío de mail"><i
                                class="far fa-envelope"></i></a></div>
                    <div class="item-contact"><a rel="noopener noreferrer" href="tel:8008320643"
                            aria-label="Llamada directa"><i class="fas fa-phone-alt"></i></a></div>
                </div>
            </div>

        </div>
        <div class="c-copy"> <i class="far fa-copyright"></i> Editorial Época 2021</div>
</footer>
</body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script defer src="https://www.googletagmanager.com/gtag/js?id=G-JJBBF478VL" crossorigin="anonymous"></script>
<script defer>
window.dataLayer = window.dataLayer || [];

function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());
gtag('config', 'G-JJBBF478VL');
</script>
<script async src="<?php uri('js'); ?>load.js" crossorigin="anonymous"></script>

</html>
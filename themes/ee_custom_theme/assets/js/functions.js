jQuery(window).scroll(function () {
  scroll = $(window).scrollTop();
  if (scroll > 1) {
    if (!flag) {
      $("header").addClass("stiky");
      flag = true;
    }
  } else {
    if (flag) {
      $("header").removeClass("stiky");
      flag = false;
    }
  }
});

var flag = false;
var scroll;


jQuery(".c-menu-icon").click(function () {
  jQuery(".c-menu-icon").toggleClass("is-active");
  jQuery(".menuppal").toggleClass("is_active");
  jQuery("header").toggleClass("is_active");
  return false;
});

jQuery(".submenu-option").click(function () {
  jQuery(".sub-menu").toggleClass("show-submenu");
});

function changeColor(color){
  var header    = jQuery('header');
  var navbar    = jQuery('.menuppal');
  var search    = jQuery('.c-search-field');
  var submenu   = jQuery('.sub-menu');

  jQuery("meta[name='theme-color']").attr('content', color);
  header.css('background', color);
  navbar.css('background', color);
  search.css('background', color);
  submenu.css('background', color);
}

var urlPath = jQuery(location).attr('pathname');

if (urlPath.includes("ediciones/nuevo-talento/")) {
  changeColor('#f76700')
} else if (urlPath.includes("ediciones/apuntes-escolares/")){
  changeColor('#e83e5a')
} else if (urlPath.includes("ediciones/clasicos-infantiles/")){
  changeColor('#833ab4')
} else if (urlPath.includes("ediciones/rtm-ediciones/")){
  changeColor('#194f9c')
} else if (urlPath.includes("ediciones/horus/")){
  changeColor('#f76700')
} else {
  changeColor('#194f9c')
}


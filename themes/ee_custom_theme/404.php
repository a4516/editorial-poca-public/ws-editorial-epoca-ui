<?php /* Template Name: 404 */ ?>
<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );?>
<?php get_header(); ?>
<main class="main-content error-page">
    <p class="error-number">404</p>
    <p class="message">No pudimos encontrar esta página</p>
    <img src="<?php uri("image") ?>404/404-people.svg" alt="404-personas-desconectando">
    <p class="invite">Te invitamos a conocer nuestras  <a href="<?php uri('home')?>/ediciones" aria-label="Pagina havia ediciones">ediciones</a></p>
</main>
<?php get_footer(); ?>

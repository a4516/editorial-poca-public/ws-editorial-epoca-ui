<?php defined('ABSPATH') or die('No script kiddies please!'); ?>
<?php get_header(); ?>
<main class="main-content search">
    <div id="primary" class="content-area">
        <div class="c-search-title">
            <?php
            $name = get_search_query();
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => 12,
                'name' => $name,
            );
            $loop = new WP_Query($args);

            if ($loop->have_posts()) {
                printf(__('<h1 class="search-title">' . 'Resultado encontrados para: %s'), '<span>' . $name . '</span>' . '</h1>');
            } else {
                printf(__('<h1 class="search-title">' . 'No se encontraron resultados para: %s'), '<span>' . $name . '</span>' . '</h1>');
            ?>
                <img class="no-results-image" src="<?php uri("image") ?>search/people-asking.svg" alt="">
            <?php
            }
            ?>
        </div>
        <ul class="products">
            <?php
            if ($loop->have_posts()) {
                while ($loop->have_posts()) : $loop->the_post();
                    wc_get_template_part('content', 'product');
                endwhile;
            }

            wp_reset_postdata();
            ?>
        </ul>
        <!--/.products-->
    </div><!-- #primary -->
</main>
<?php get_footer(); ?>
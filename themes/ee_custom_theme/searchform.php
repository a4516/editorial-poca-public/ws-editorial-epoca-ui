<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
<i class="fas fa-search"></i>
<div class="mdl-textfield mdl-js-textfield">
        <input class="mdl-textfield__input" type="text" id="txtSearch" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label') ?>">
        <label class="mdl-textfield__label" for="txtSearch"><label>
</div>
    
    <input type="submit" class="search-submit" value="<?php echo esc_attr_x('Buscar', 'submit button') ?>" />
</form>
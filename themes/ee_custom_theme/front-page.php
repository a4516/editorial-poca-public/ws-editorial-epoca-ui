<?php /* Template Name: Inicio */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>
<?php get_header(); ?>
<main class="main-content home">
    <div class="c-main-banner">
        <div class="c-banner-text">
            <h1>"En Editorial Época un lector <br><span>vive mil vidas antes de morir"</span> </h1>
            <a href="<?php echo home_url(); ?>/ediciones" aria-label="Página de ediciones"><i class="fas fa-books"></i> Descubre nuestros
                ejemplares</a>
        </div>
        <div class="c-banner-image">
        </div>
    </div>
    <div class="c-lasted-added">
        <div class="c-title">Agregados recientemente</div>
        <div class="c-lasted-aded-slider">
            <?php echo do_shortcode('[wpb-product-slider orderby="date" order="DESC"]'); ?>
        </div>
    </div>
    <div class="c-lasted-added hidden">
        <div class="c-title">Más visto</div>
        <div class="clasted-aded-slider">
            <?php echo do_shortcode('[product_categories number="0" parent="0"]'); ?>
        </div>
    </div>
    <div class="c-about-us">
        <div class="c-title">
            <h2>¿Por qué elegirnos? Conócenos</h2>
        </div>
        <div class="c-items-about-us">
            <div class="about-us-item">
                <div class="about-us-item-image reloj-de-arena"></div>
                <h3>Tenemos una amplia experiencia en el mercado</h3>
            </div>
            <div class="about-us-item">
                <div class="about-us-item-image libro"></div>
                <h3>Contamos con un catálogo muy extenso</h3>
            </div>
            <div class="about-us-item">
                <div class="about-us-item-image evolucion"></div>
                <h3>Hacemos crecer nuestros catálogos constantemente</h3>
            </div>
            <div class="about-us-item">
                <div class="about-us-item-image mundo"></div>
                <h3>Contamos con envíos nacionales y al extranjero</h3>
            </div>
        </div>
    </div>
    <div class="c-owr-clients">
        <div class="c-title">
            <h2>Nuestros clientes</h2>
        </div>
        <div class="owr-clients-items">
            <div class="owr-clients-item gandhi"></div>
            <div class="owr-clients-item el-sotano"></div>
            <div class="owr-clients-item walmart"></div>
            <div class="owr-clients-item gonvill"></div>
            <div class="owr-clients-item tony"></div>
            <div class="owr-clients-item chedraui"></div>
        </div>
    </div>
    <div class="c-distribuidores">
        <div class="c-distribuidroes-info">
            <p>La lectura está en todas partes pero no en todos esta el gusto por la lectura,
                si eres de los pocos a los que les encanta el olor de un libro nuevo te invitamos
                a llenarte de experiencias que te envolverán en un mundo fantástico</p>
            <a href="<?php echo home_url(); ?>/nuestros-distribuidores" aria-label="Pagina de nuestros distribuidores"><i class="fas fa-user-check"></i> Visita nuestros
                distribuidores</a>
        </div>
        <div class="c-distribuidores-image">
        </div>
    </div>
</main>
<?php get_footer(); ?>
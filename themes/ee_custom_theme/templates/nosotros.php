<?php /* Template Name: Nosotros */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<?php get_header(); ?>
<main class="main-content nosotros">
    <div class="c-nosotros">
        <h1>Nuestra historia</h1>

        <div class="c-information">
            <div class="c-image">
                <img src="<?php uri("image") ?>base-picture.png" alt="">
            </div>
            <div class="c-descrition">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum libero ut autem animi ipsam aperiam ad amet suscipit tempore recusandae. Quaerat, inventore numquam voluptatum iste dolorum id tempora architecto ratione.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit pariatur facilis quia expedita esse perspiciatis, ab et reiciendis quisquam numquam ullam, at id ea necessitatibus ducimus hic quibusdam fuga. Ratione.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea blanditiis soluta incidunt, inventore alias quaerat, libero architecto tempora, illum temporibus quas voluptatum sequi. Aperiam eaque eius impedit molestias, cupiditate modi.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus, similique harum. Inventore commodi omnis suscipit praesentium est numquam qui architecto fugit eveniet ratione reprehenderit voluptatem molestiae, nesciunt officia ipsum! Reiciendis.</p>
                <p>Natus, aliquam maxime ratione praesentium rerum deleniti dolorem impedit dicta obcaecati maiores ipsum, quod laborum ex rem, et eius eum dolore quae numquam cumque officia error eveniet voluptatem labore? Pariatur!</p>
                <p>Vitae pariatur commodi delectus, voluptatibus doloribus ipsam consequatur quibusdam labore iure officiis neque repudiandae dolore perspiciatis assumenda dolorem fugiat alias praesentium suscipit ex enim est, earum rerum perferendis. Pariatur, molestias.</p>
            </div>
        </div>
    </div>

</main>
<?php get_footer(); ?>
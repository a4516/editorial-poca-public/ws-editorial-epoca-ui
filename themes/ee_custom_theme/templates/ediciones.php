<?php /* Template Name: Ediciones */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<?php get_header(); ?>
<main class="main-content ediciones">
    <div class="c-ediciones-slider-main c-slider hidden">
        <div class="c-slide-item">
            <img data-lazy="<?php uri("image") ?>ediciones/slider/banner_1.png" alt="">
        </div>
        <div class="c-slide-item">
            <img data-lazy="<?php uri("image") ?>ediciones/slider/banner_2.png" alt="">
        </div>
        <div class="c-slide-item">
            <img data-lazy="<?php uri("image") ?>ediciones/slider/banner_3.png" alt="">
        </div>
    </div>
    <div class="c-ediciones-main">
        <div class="c-title">
            <h1>Todas nuestras ediciones</h1>
        </div>
        <div class="ediciones-items">
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/editorial-epoca.svg" alt="">
                <div class="info">
                    <h2>Ediorial Época</h2>
                    <p>Edición enfocada en las colecciones de poesía, teatro, clásicos universales 
                        y obras de todas las épocas.</p>
                    <a href="<?php echo home_url(); ?>/ediciones/editorial-epoca/" aria-label="Pagina hacia ediciones de la editorial"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/horus.svg" alt="">
                <div class="info">
                    <h2>HORUS</h2>
                    <p>Lectura mística, saludable, divertida y cognitiva. Conoce temas de tu interés y 
                        nunca dejes de aprender. </p>
                    <a href="<?php echo home_url(); ?>/ediciones/horus" aria-label="Pagina hacia ediciones HORUS"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/apuntes-escolares.svg" alt="">
                <div class="info">
                    <h2>Apuntes Escolares</h2>
                    <p>Indaga entre relatos clásicos de aventura, romance, drama, historia y mucho más, 
                        en la edición Apuntes Escolares, encuentra o descubre obras extraordinarias.</p>
                    <a href="<?php echo home_url(); ?>/ediciones/apuntes-escolares" aria-label="Pagina hacia Apuntes Escolares"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/clasicos-infantiles.svg" alt="">
                <div class="info">
                    <h2>Clásicos Infantiles</h2>
                    <p>Edición dedicada a los cuentos más populares de la infancia para compartir relatos de 
                        aventura y diversión, es momento de crear, diseñar e inventar formas creativas de contar 
                        historias.</p>
                    <a href="<?php echo home_url(); ?>/ediciones/clasicos-infantiles" aria-label="Pagina hacia Clásicos Infantiles"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/nuevo-talento.svg" alt="">
                <div class="info">
                    <h2>Nuevo Talento</h2>
                    <p>Obras de todos los géneros, para todas las edades. Una edición donde encontrarás aventuras 
                        impresionantes relatadas por mentes brillantes.</p>
                    <a href="<?php echo home_url(); ?>/ediciones/nuevo-talento" aria-label="Pagina hacia Nuevo Talento"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
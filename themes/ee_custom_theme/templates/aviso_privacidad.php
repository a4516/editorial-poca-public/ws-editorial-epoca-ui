<?php /* Template Name: Aviso legal y condiciones de uso*/ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<?php get_header(); ?>
<main class="main-content aviso-legal">
    <h1>AVISO LEGAL Y CONDICIONES DE USO</h1>
    <p>Editorial Época S.A de C.V (en adelante «Editorial Época»), es titular de la página 
    web https://editorialepoca.mx/(En adelante el “Sitio Web”).
    </p>

    <h2>ACCESO Y CONDICIONES DE USO DEL SITIO WEB</h2>
    <p>Las presentes Condiciones de Uso (En adelante las “Condiciones”), tienen por finalidad facilitar al usuario
        información relativa a Editorial Época, sus actividades y productos, noticias publicadas, así como sus distintas
        áreas de
        negocio.</p>
    <p>Este Sitio Web tiene carácter informativo.</p>
    <p>Tanto la navegación, como el acceso a la información publicada en el Sitio Web, suponen la aceptación como
        usuario,
        sin reservas de ninguna clase, de todas y cada una de las presentes condiciones. Si no acepta las presentes
        condiciones, le rogamos se abstenga de utilizar el Sitio Web y su contenido.</p>
    <p>Editorial Época podrá, en todo momento y sin previo aviso, modificar las presentes condiciones, todo ello expuesto
        en el
        Sitio Web, mediante la publicación de dichas modificaciones en el mismo, con el fin de que puedan ser
        conocidas por
        los usuarios, siempre antes de la visita al Sitio Web.</p>
    <p>Editorial Época podrá denegar el acceso al Sitio Web a los usuarios que hagan un mal uso de los contenidos y/o
        incumplan
        cualquiera de las condiciones que aparecen en el presente documento.</p>

    <p><b>Acceso y Utilización</b></p>
    <p>El acceso al Sitio Web es gratuito, salvo en lo relativo al coste de la conexión a través de la red de
        telecomunicaciones suministrada por el proveedor de acceso contratado por el usuario.</p>
    <p>El usuario se compromete a hacer un uso diligente del Sitio Web, así como de la información contenida en el
        mismo, con total sujeción tanto a la normativa aplicable, como a las presentes condiciones.</p>

    <p></b>Obligación de hacer un uso correcto</b></p>
    <p>Los usuarios son íntegramente responsables de su conducta, al acceder a la información del Sitio, mientras
        naveguen o accedan en las mismas, así como después de haber accedido.</p>
    <p>El usuario se compromete a la correcta utilización del Sitio Web y utilidades que se le proporcionen conforme
        a la ley, las presentes condiciones, las instrucciones y avisos que se le comuniquen, así como con la moral y las
        buenas costumbres generalmente aceptadas y al orden público.</p>
    <p>El usuario se obliga al uso exclusivo del Sitio Web, así como todos sus contenidos, para fines lícitos y no
        prohibidos, que no infrinjan la legalidad vigente y/o puedan resultar lesivos de los derechos legítimos de
        Editorial Época o de cualquier tercero y/o que puedan causar cualquier daño o perjuicio de forma directa o 
        indirecta.</p>
    <p>De conformidad con lo dispuesto anteriormente, se entenderá por contenido sin que esta enumeración tenga
        carácter
        limitativo: los textos, fotografías, gráficos, imágenes, iconos, tecnología, software, links y demás
        contenidos
        audiovisuales o sonoros, así como su diseño gráfico y códigos fuente (en adelante, todo ello el/los
        «Contenido/s»),
        de conformidad con la ley, las presentes condiciones, los demás avisos, reglamentos de uso e instrucciones
        puestos
        en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público,
        y, en
        particular, se compromete a abstenerse de: reproducir, copiar, distribuir, poner a disposición o de
        cualquier otra
        forma comunicar públicamente, transformar o modificar los contenidos, a menos que se cuente con la
        autorización del
        titular de los correspondientes derechos o ello resulte legalmente permitido; o se realice, en su caso, por
        los
        botones habilitados por Editorial Época para redes sociales. En este sentido, el usuario no podrá suprimir,
        manipular o de
        cualquier forma alterar el «copyright» y demás datos identificativos de la reserva de derechos de Editorial Época
        o de
        cualesquiera otros medios técnicos establecidos para su reconocimiento.</p>
    <p>El usuario se compromete, a título meramente enunciativo y no limitativo, a no transmitir, difundir o poner a
        disposición de terceras informaciones, datos, contenidos, mensajes, gráficos, dibujos, archivos de sonido
        y/o
        imagen, fotografías, grabaciones, software y, en general, cualquier clase de material propiedad de este
        Sitio Web,
        así como a abstenerse de realizar actos, que:</p>
    <p>– Induzcan, inciten o promuevan actuaciones delictivas, difamatorias, violentas o, en general, contrarias a
        la Ley, a
        la moral y buenas costumbres generalmente aceptadas o al orden público;</p>
    <p>– Sean falsos, ambiguos, inexactos, exagerados o extemporáneos, de forma que induzcan o puedan inducir a
        error sobre
        su objeto o sobre las intenciones o propósitos del comunicante;</p>
    <p>– Se encuentren protegidos por cualesquiera derechos de propiedad intelectual o industrial pertenecientes a
        terceros,
        sin que el Usuario haya obtenido previamente de sus titulares la autorización necesaria para llevar a cabo
        el uso
        que efectúa o pretende efectuar;</p>
    <p>– Constituyan, en su caso, publicidad ilícita, engañosa o desleal y, en general, que constituyan competencia
        desleal
        o vulneren las normas reguladoras de la protección de datos de carácter personal.</p>
    <p>– Incorporen virus u otros elementos físicos o electrónicos que puedan dañar o impedir el normal
        funcionamiento de la
        red, del sistema o de equipos informáticos (hardware y software) de Editorial Época.</p>
    <p>– Provoquen por sus características (tales como formato o extensión, entre otras) dificultades en el normal
        funcionamiento de los servicios ofrecidos por este Sitio Web.</p>
    <p>El usuario responderá de los daños y perjuicios de toda naturaleza que Editorial Época, pueda sufrir como
        consecuencia del
        incumplimiento de cualesquiera de las obligaciones a las que queda sometido en virtud de las presentes
        Condiciones o
        de la legislación aplicable en relación con la utilización del Sitio Web.</p>

    <h2>EXONERACIÓN DE RESPONSABILIDAD DE FLUIDRA</h2>
    <p>A título enunciativo, pero no limitativo, Editorial Época no asumirá responsabilidad alguna:</p>
    <p>– De la utilización que los usuarios puedan hacer de los materiales de este Sitio Web, y/o webs de enlace, ya
        sean
        prohibidos o permitidos, en infracción de los derechos de propiedad intelectual y/o industrial de contenidos
        de la
        web o de terceros.</p>
    <p>– De los eventuales daños y perjuicios a los usuarios causados por un funcionamiento normal o anormal de las
        herramientas de búsqueda, de la organización o la localización de los contenidos y/o acceso al Sitio Web y,
        en
        general, de los errores o problemas que se generen en el desarrollo o instrumentación de los elementos
        técnicos que
        el Sitio Web o un programa facilite al usuario.</p>
    <p>– De los contenidos de aquellas páginas a las que los usuarios puedan acceder desde enlaces incluidos en el
        Sitio
        Web, ya sean autorizados o no.</p>
    <p><b>Editorial Época informa que no garantiza:</b></p>
    <p>– Que el acceso al Sitio Web, y/o a las webs de enlace sea ininterrumpido o de error.</p>
    <p>– Que el contenido o software al que los usuarios accedan a través del Sitio Web, o de las webs de enlace no
        contenga
        error alguno, virus informático u otros elementos en los contenidos que puedan producir alteraciones en su
        sistema o
        en los documentos electrónicos y ficheros almacenados en su sistema informático o cause otro tipo de daño;
    </p>
    <p>La información contenida en el Sitio Web debe ser considerada por los Usuarios como divulgativa y
        orientadora, tanto
        con relación a su finalidad como a sus efectos, motivo por el cual:</p>

    <p>Editorial Época no garantiza la exactitud de la información contenida en el Sitio Web y por consiguiente no asumen
        responsabilidad alguna sobre los posibles perjuicios o incomodidades para los usuarios que pudiesen
        derivarse alguna
        inexactitud presente en el mismo.</p>
    <h2>POLÍTICA DE PRIVACIDAD: PROTECCIÓN DE DATOS PERSONALES</h2>
    <p>Los usuarios se comprometen a navegar por la página web y a utilizar el contenido de la misma de buena fe.
    </p>
    <p>En cumplimiento de lo establecido en la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal,
        informamos
        que, los datos de carácter personal incorporados para la cumplimentación de cualquier formulario existente
        en este
        Sitio Web o la remisión de un correo electrónico a cualquiera de nuestros buzones:</p>
    <p>1º.- Serán tratados con la máxima confidencialidad y formarán parte de los ficheros titularidad de FLUIDRA
        para su
        gestión y dar respuesta a los mismos, así como, para la gestión comercial que los vincule, y el envío a
        través de
        email de las solicitudes de información que se realicen a través de este Sitio Web.</p>
    <p>2º.- Implica la aceptación de esta política de privacidad, así como la autorización a Editorial Época para que
        trate los
        datos personales que se le faciliten.</p>
    <p>Por la mera visita al Sitio Web, los usuarios no facilitan información personal alguna ni queda obligado a
        facilitarla.</p>
    <p>Conforme a la legislación vigente en materia de protección de datos, Editorial Época ha adoptado los niveles de
        seguridad
        adecuados a los datos facilitados por los usuarios y, además, ha instalado todos los medios y medidas a su
        alcance
        para evitar la pérdida, mal uso, alteración, acceso no autorizado y extracción de los mismos.</p>
    <p><b>Derechos de acceso, rectificación, cancelación y oposición</b></p>
    <p>En cualquier momento y conforme establece la citada Ley Orgánica, los usuarios podrán ejercitar los derechos
        de
        acceso, rectificación, cancelación y oposición de sus datos personales, mediante petición escrita dirigida a
        la
        dirección de correo electrónico contacto@editorialepoca.mx o a la dirección postal Av. Emperadores 185, Portales 
        Nte, Benito Juárez, 03300 Ciudad de México, CDMX, indicando según sea conveniente, de forma visible el concreto derecho que se ejerce, así
        como adjuntando copia del INE.</p>

    <h2>UTILIZACIÓN DE COOKIES</h2>
    <p>Para la utilización de este Sitio Web, es necesaria la utilización de cookies y pequeños ficheros de datos.
        Las
        cookies se utilizan con la finalidad de mejorar el servicio prestado por Editorial Época y son aceptadas por el
        Usuario en
        el momento de acceder al Sitio Web. En ningún caso se almacenará ningún tipo de información personal
        relativa a los
        usuarios y toda la información obtenida será anónima. Si el usuario lo desea, puede configurar su navegador
        para
        impedir la instalación de cookies en su disco duro, para lo cual le aconsejamos que consulte las
        instrucciones y
        manuales de su navegador.</p>
    <h2>PROPIEDAD INTELECTUAL E INDUSTRIAL</h2>
    <p>Editorial Época ostenta todos los derechos sobre el contenido, diseño y código fuente del Sitio Web y, en especial,
        con
        carácter enunciativo, pero no limitativo, sobre las fotografías, imágenes, textos, logos, diseños, marcas,
        nombres
        comerciales y datos que se incluyen en la web o aplicación.</p>
    <p>Se advierte a los usuarios de que tales derechos están protegidos por la legislación vigente española e
        internacional
        relativa a la propiedad intelectual e industrial.</p>
    <p>Queda expresamente prohibida la reproducción total o parcial del Sitio Web, ni siquiera mediante un
        hiperenlace, ni
        de cualquiera de sus contenidos, sin el permiso expreso y por escrito de Editorial Época.</p>
    <p>Asimismo, queda totalmente prohibida la copia, reproducción, adaptación, modificación, distribución,
        comercialización, comunicación pública y/o cualquier otra acción que comporte una infracción de la normativa
        vigente
        española y/o internacionales en materia de propiedad intelectual y/o industrial, así como el uso de los
        contenidos
        de la web o aplicación si no es con la previa autorización expresa y por escrito de Editorial Época.</p>
    <p>Editorial Época informa que no concede licencia o autorización implícita alguna sobre los derechos de propiedad
        intelectual
        y/o industrial o sobre cualquier otro derecho o propiedad relacionada, directa o indirectamente, con los
        contenidos
        incluidos en la web o aplicación.</p>

    <h2>GUÍA DE USO SEGURO:</h2>

    <p><b>Phishing</b></p>
    <p>No utilice enlaces incorporados en e-mails o páginas Web de terceros para acceder a este Sitio Web.
        Periódicamente se
        detectan envíos de correos masivos indiscriminados, remitidos desde direcciones electrónicas falsas, con el
        único
        objetivo de conseguir información confidencial de los usuarios. A esta técnica se la conoce con el nombre de
        «PHISHING». Editorial Época declina cualquier responsabilidad ajena a este respecto.</p>
    <h2>LEGISLACIÓN APLICABLE Y JURISDICCIÓN</h2>
    <p>El acceso y utilización del Sitio Web se regirá e interpretará de conformidad con la legislación mexicana.
        Cualquier
        controversia que pudiera surgir entre Editorial Época y los usuarios del Sitio Web, será dirimida, con renuncia
        expresa de
        las Partes a su propio fuero, por los Juzgados y Tribunales de México.</p>

</main>
<?php get_footer(); ?>
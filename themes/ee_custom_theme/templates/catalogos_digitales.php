<?php /* Template Name: Catálogos digitales */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<?php get_header(); ?>
<main class="main-content catalogos-digitales">

    <div class="c-title-catalogos-digitales">
        <h1>Catálogos Digitales</h1>
    </div>
    <div class="c-catalogos-digitales-main">
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-apuntes-escolares.webp" alt="">
                <div class="info">
                    <h2>Apuntes Escolares</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_AE_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" data-izimodal-open="#modal-AP" data-izimodal-transitionin="fadeInDown" class="modal-iframe apuntesEscolares">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-clasicos-infantiles.webp" alt="">
                <div class="info">
                    <h2>Clásicos Infantiles</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_CI_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" data-izimodal-open="#modal-CI" data-izimodal-transitionin="fadeInDown" class="modal-iframe clasicosInfantiles">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-diviertete-aprende.webp" alt="">
                <div class="info">
                    <h2>Diviertete y Aprende</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_DYA_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" data-izimodal-open="#modal-DYA" data-izimodal-transitionin="fadeInDown" class="modal-iframe divierteteAprende">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-editorial-epoca.webp" alt="">
                <div class="info">
                    <h2>Editorial Época</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_EE_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" data-izimodal-open="#modal-EE" data-izimodal-transitionin="fadeInDown" class="modal-iframe editorialEpoca">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-horus.webp" alt="">
                <div class="info">
                    <h2>Ediciones HORUS</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_H_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" data-izimodal-open="#modal-H" data-izimodal-transitionin="fadeInDown" class="modal-iframe horus">

                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-nuevo-talento.webp" alt="">
                <div class="info">
                    <h2>Nuevo Talento</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_NT_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                            <a href="#" data-izimodal-open="#modal-NT" data-izimodal-transitionin="fadeInDown" class="modal-iframe nuevoTalento">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-rtm.webp" alt="">
                <div class="info">
                    <h2>Ediciones RTM</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_RTM_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" data-izimodal-open="#modal-RTM" data-izimodal-transitionin="fadeInDown" class="modal-iframe rtm">

                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-AP" class="c-pdf-modal-apuntesEscolares"></div>
    <div id="modal-CI" class="c-pdf-modal-clasicosInfantiles"></div>
    <div id="modal-DYA" class="c-pdf-modal-divierteteAprende"></div>
    <div id="modal-EE" class="c-pdf-modal-editorialEpoca"></div>
    <div id="modal-H" class="c-pdf-modal-horus"></div>
    <div id="modal-NT" class="c-pdf-modal-nuevoTalento"></div>
    <div id="modal-RTM" class="c-pdf-modal-rtm"></div>



</main>
<?php get_footer(); ?>
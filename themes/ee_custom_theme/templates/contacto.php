<?php /* Template Name: Contactanos */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<?php get_header(); ?>
<main class="main-content contacto">
    <div class="c-contacto">
        <div class="c-contacto-form">
            <form id="contact-form">
                <div class="c-form-header">
                    <p>Contáctanos</p>
                </div>
                <div class="c-form-container">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="txtName" minlength="10" name="txtName" required>
                            <label class="mdl-textfield__label" for="txtName">Nombre Completo*</label>
                            <img src="<?php uri("image") ?>/contacto/usuario.svg" alt="" class="icon-form">
                        </div>
                        <div class="m-contacto">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label email">
                                <input class="mdl-textfield__input" type="email" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,7}" id="txtMail" name="txtMail" required>
                                <label class="mdl-textfield__label" for="txtMail">Correo Electrónico*</label>
                                <img src="<?php uri("image") ?>/contacto/mail.svg" alt="" class="icon-form">
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="tel" id="tel" pattern="[0-9]{1,10}" minlength="10" name="txtPhoneNumber">
                                <label class="mdl-textfield__label" for="txtPhoneNumber">Número celular (opcional)</label>
                                <img src="<?php uri("image") ?>/contacto/phone.svg" alt="" class="icon-form">
                            </div>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label no-icon-input">
                            <select class="mdl-textfield__input" id="listAsunto" name="listAsunto" required>
                                <option></option>
                                <option value="QA">Deseo poner una queja o aclaración</option>
                                <option value="SI">Deseo solicitar información</option>
                                <option value="SC">Deseo solicitar una cotización</option>
                                <option value="FE">Deseo felicitarlos</option>
                            </select>
                            <label class="mdl-textfield__label" for="listAsunto">Asunto*</label>
                        </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label no-icon-input">
                        <textarea class="mdl-textfield__input" type="text" rows="3" id="txtComentario" minlength="15" name="txtComentario" required></textarea>
                        <label class="mdl-textfield__label" for="txtComentario">Comentarios*</label>
                        <span class="mdl-textfield__error">Los comentarios deben ser de un mínimo de 15 caracteres</span>
                    </div>
                    <div class="c-button">
                        <button> <i class="fas fa-paper-plane"></i> Enviar</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="c-extra-information">
            <div class="c-horarios">
                <p class="card-title"><i class="far fa-clock"></i> Horarios </p>
                <ul>
                    <li>Lunes a viernes de 8:00 a 19:00hrs</li>
                    <li>Sábado de 8:00 a 14:00hrs</li>
                </ul>
            </div>
            <div class="c-direccion">
                <p class="card-title"><i class="fas fa-map-marker-alt"></i> Dirección</p>
                <a href="https://www.google.com/maps/place/EDITORIAL+EPOCA+SA+DE+CV/@19.3673252,-99.155609,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1ff1007cd6e0d:0xfcbbebc5462e05de!8m2!3d19.367331!4d-99.1534186" target="_blank" rel="noopener noreferrer" aria-label="Localización con google maps">
                    Av. Emperadores 185, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX <i class="fas fa-external-link-alt"></i>
                </a>
            </div>
            <div class="c-tels">
                <p class="card-title"><i class="fas fa-phone-alt"></i>Teléfonos</p>
                <p><a href="tel:+8008320643" aria-label="Llamada directa - Linea 1">800-832-06-43</a></p>
                <p><a href="tel:+5556049072" aria-label="Llamada directa - Linea 2">55-5604-9072</a></p>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
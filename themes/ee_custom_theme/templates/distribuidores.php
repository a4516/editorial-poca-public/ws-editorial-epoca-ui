<?php /* Template Name: Nuestros distribuidores */ ?>
<?php defined('ABSPATH') or exit('No script kiddies please!'); ?>
<?php get_header(); ?>
<main class="main-content distribuidores">
    <div class="c-distribuidores">
        <div class="distribuidores-header">
            <div class="distribuidores-header-elements">
                <div class="d-title-container">
                    <h1>Nuestros distribuidores</h1>
                </div>
                <div class="d-header-text">
                    <p>Para la lectura no hay límites, queremos compartir contigo las mejores experiencias  
                        literarias; es por eso que nuestros distribuidores están localizados estratégicamente 
                        para que estén al alcance de todos. Si estás en busca de los mejores libros, consulta 
                        los distribuidores cercanos a ti y sabrás dónde encontrarlos. </p>
                </div>
            </div>
        </div>
        <div class="c-distribuidores-items">
        </div>
    </div>
</main>
<script>
    var distribuidoresArray = [
        {
            "nombreEditorial": "EDITORIAL IZTACCIHUATL DE MONTERREY, S.A. DE C.V.",
            "direccion": "QUIMICOS # 120 COL. TECNOLOGICO C.P. 64700, MONTERREY, N.L.",
            "linkMaps": "https://www.google.com.mx/maps/place/Librer%C3%ADas+Iztacc%C3%ADhuatl/@25.6492121,-100.296471,17z/data=!3m1!4b1!4m5!3m4!1s0x8662bfbe92891cf7:0xbdee4885465a2fe2!8m2!3d25.6492292!4d-100.2943016",
            "phoneNumber": "",
            "email" : "",
        },
        {
            "nombreEditorial": "LIBRERÍA MADERO DE MORELIA, S.A. DE C.V.",
            "direccion": "MADERO ORIENTE NO. 338-A COL. CENTRO, C.P. 58000 MORELIA, MICH.",
            "linkMaps": "https://www.google.com.mx/maps/place/Libreria+de+libros+Madero+De+Morelia+Sa+De+Cv/@19.7027315,-101.1904483,17z/data=!3m1!4b1!4m5!3m4!1s0x842d0e72494b5831:0x92fff9ba2f1e3e83!8m2!3d19.7027584!4d-101.1882584",
            "phoneNumber": "014433121609",
            "email" : "",
        },
        {
            "nombreEditorial": "LIBERIA LA CATEDRAL",
            "direccion": "HIDALGO No. 132 COL. CENTRO 37000, LEON. GTO.",
            "linkMaps": "https://www.google.com.mx/maps/place/Librer%C3%ADa+Catedral+de+Le%C3%B3n/@21.1231873,-101.68458,17z/data=!3m2!4b1!5s0x842bbf0c976c662b:0xdd0f724cbd70e625!4m5!3m4!1s0x842bbf0c9837b1a1:0xade1807c077ccee0!8m2!3d21.1231823!4d-101.6823913",
            "phoneNumber": "",
            "email" : "",
        },
        {
            "nombreEditorial": "JOSE LOPEZ TEJERA / LIB. CIENTIFICA",
            "direccion": "AVE. 20 DE NOVIEMBRE #558 COL. CENTRO, C.P. 91700 VERACRUZ, VERACRUZ",
            "linkMaps": "https://www.google.com.mx/maps/place/Av+20+de+Noviembre+558,+Salvador+D%C3%ADaz+Mir%C3%B3n,+91700+Veracruz,+Ver./@19.1896821,-96.1353586,17z/data=!3m1!4b1!4m5!3m4!1s0x85c346b64b3afa2d:0x8c2f1d5425bb857d!8m2!3d19.189677!4d-96.1331699",
            "phoneNumber": "2299329053",
            "email" : "",
        },
        {
            "nombreEditorial": "LIBRERIAS DANTE, S.A. DE C.V.",
            "direccion": "CALLE 19 Nº 102 x 20 COL. MEXICO 97125, MERIDA, YUCATAN",
            "linkMaps": "https://www.google.com.mx/maps/place/Librer%C3%ADa+Dante+20+x+21/@20.9901386,-89.6354287,13z/data=!4m9!1m2!2m1!1sLIBRERIAS+DANTE,+S.A.+DE+C.V.!3m5!1s0x8f5677902f1e45cf:0x31de26fe5ad5efa0!8m2!3d21.002915!4d-89.610665!15sCh1MSUJSRVJJQVMgREFOVEUsIFMuQS4gREUgQy5WLiIDiAEBWhoiGGxpYnJlcmlhcyBkYW50ZSBzYSBkZSBjdpIBCmJvb2tfc3RvcmU",
            "phoneNumber": "9999441985",
            "email" : "",
        },
        {
            "nombreEditorial": "EL ESCRITORIO MODERNO, S.A. DE C.V.",
            "direccion": "3ª AVENIDA SUR ORIENTE # 748 COL. CENTRO 29000, TUXTLA GTZ, CHIAPAS",
            "linkMaps": "https://www.google.com.mx/maps/place/3a+Avenida+Sur+Ote.+748,+Asamblea+de+barrio,+San+Roque,+29000+Tuxtla+Guti%C3%A9rrez,+Chis./@16.7501037,-93.1132653,17z/data=!3m1!4b1!4m5!3m4!1s0x85ecd889a718d12d:0xca6f224388ec04d!8m2!3d16.7500986!4d-93.1110766",
            "phoneNumber": "",
            "email" : "",
        },
        {
            "nombreEditorial": "JUAN COLORADO CARDONA O MI LIBRERÍA",
            "direccion": "5 DE MAYO # 255 COL. CENTRO 78000, SAN LUIS POTOSI",
            "linkMaps": "https://www.google.com.mx/maps/place/Mi+Librer%C3%ADa/@22.1503425,-100.9786543,17z/data=!3m2!4b1!5s0x842aa20197c251a9:0xd95184e942a2f5b5!4m5!3m4!1s0x842aa201a2969cc9:0x5a3b30728a5253f8!8m2!3d22.1503375!4d-100.9764656",
            "phoneNumber": "",
            "email" : "",
        },
        {
            "nombreEditorial": "LIB. UNIVERSAL DE ZACATECAS",
            "direccion": "HIDALGO Nº 109 COL.CENTRO 98000, ZACATECAS, ZACATECAS",
            "linkMaps": "https://www.google.com.mx/maps/place/LIBRER%C3%8DA+UNIVERSAL/@22.7724514,-102.5767203,17z/data=!3m1!4b1!4m5!3m4!1s0x86824e88390f3ad1:0x9908cf740ea9f11b!8m2!3d22.7724429!4d-102.5745235",
            "phoneNumber": "014929241240",
            "email" : "",
        },
        {
            "nombreEditorial": "Pasaje Zócalo-Pino Suárez",
            "direccion": "Metro de la Ciudad de México Local 29",
            "linkMaps": "https://www.google.com.mx/maps/place/P.za+de+la+Constituci%C3%B3n+%26+Jos%C3%A9+Mar%C3%ADa+Pino+Su%C3%A1rez,+Centro+Hist%C3%B3rico+de+la+Cdad.+de+M%C3%A9xico,+Centro,+06060+Ciudad+de+M%C3%A9xico,+CDMX/@19.4332763,-99.1330565,18.63z/data=!4m5!3m4!1s0x85d1fecd32f5a033:0x10f58a758099c7a8!8m2!3d19.4332972!4d-99.1324443",
            "phoneNumber": "5555223130",
            "email" : "",
        },
        {
            "nombreEditorial": "EDICIONES EPOCA",
            "direccion": "Allende # 14 Col. Centro, Esq. Donceles Ciudad de México",
            "linkMaps": "https://www.google.com.mx/maps/place/Ignacio+Allende+14,+Centro+Hist%C3%B3rico+de+la+Cdad.+de+M%C3%A9xico,+Centro,+Cuauht%C3%A9moc,+06000+Ciudad+de+M%C3%A9xico,+CDMX/@19.4369891,-99.1400982,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1f92c4fbe8e55:0xb4a219515ad31a71!8m2!3d19.4369841!4d-99.1379095",
            "phoneNumber": "5555127605",
            "email" : "",
        },
        {
            "nombreEditorial": "LIBRERIA BELLAS ARTES",
            "direccion": "Avenida Juárez Col.Centro 06050 Cuahutemoc, CDMX",
            "linkMaps": "https://www.google.com.mx/maps/place/Librer%C3%ADa+El+S%C3%B3tano+Bellas+Artes/@19.4342654,-99.1445772,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1f8d5aae8f537:0x76e346d95e489597!8m2!3d19.4342604!4d-99.1423885",
            "phoneNumber": "5555102596",
            "email" : "", 
        },
        {
            "nombreEditorial": "LIBRERIA DEL SOTANO COYOACAN",
            "direccion": "Ignacio Allende 38, Del Carmen, Coyoacán, 04100 Ciudad de México, CDMX",
            "linkMaps": "https://www.google.com.mx/maps/place/Librer%C3%ADa+El+S%C3%B3tano+Quevedo/@19.3463707,-99.2498245,12z/data=!4m9!1m2!2m1!1sLIBRERIA+DEL+SOTANO+COYOACAN+cdmx!3m5!1s0x85d1fffac214f6eb:0x82e6e48fb52614df!8m2!3d19.3464579!4d-99.1797568!15sCiFMSUJSRVJJQSBERUwgU09UQU5PIENPWU9BQ0FOIGNkbXgiA4gBAVojIiFsaWJyZXJpYSBkZWwgc290YW5vIGNveW9hY2FuIGNkbXiSAQpib29rX3N0b3Jl",
            "phoneNumber": "",
            "email" : "", 
        },
        {
            "nombreEditorial": "FONDO DE CULTURA ECONOMICA",
            "direccion": "CARRETERA PICACHO AJUSCO Col. BOSQUES DEL PEDREGAL 14738 DELEG.TLALPAN CDMX",
            "linkMaps": "https://www.google.com.mx/maps/place/Fondo+De+Cultura+Econ%C3%B3mica/@19.3027469,-99.2109056,17z/data=!3m1!4b1!4m5!3m4!1s0x85cdfe40900fddbf:0x2902ac1ec62c1324!8m2!3d19.3027419!4d-99.2087169",
            "phoneNumber": "5552274681",
            "email" : "", 
        },
        //{    NO UBICADO
        //    "nombreEditorial": "JAIME GELIS VILA ",
        //    "direccion": "TORRES ADALID Col. NARVARTE ORIENTE 03023 BENITO JUAREZ, CDMX",
        //    "linkMaps": "",
        //    "phoneNumber": "",
        //    "email" : "", 
        //},
        {
            "nombreEditorial": "LIBRERIA DE PORRUA HERMANOS Y COMPAÑIA",
            "direccion": "REPUBLICA DE ARGENTINA Col. CENTRO 06020 DELEG.CUAUHTEMOC CDMX				",
            "linkMaps": "https://www.google.com.mx/maps/place/Librer%C3%ADa+Porr%C3%BAa/@19.4355754,-99.1320027,19.75z/data=!3m1!5s0x85d1f933035d3d47:0xaa98c45fbd7e697e!4m9!1m2!2m1!1zTGlicmVyw61hIGRlIFBvcnLDumEgSGVybWFub3MgeSBDb21wYcOxw61hLCBjZG14!3m5!1s0x85d1f93302faf217:0xbe88a79353686914!8m2!3d19.4358068!4d-99.1315839!15sCjBMaWJyZXLDrWEgZGUgUG9ycsO6YSBIZXJtYW5vcyB5IENvbXBhw7HDrWEsIGNkbXgiA4gBAVoxIi9saWJyZXLDrWEgZGUgcG9ycsO6YSBoZXJtYW5vcyB5IGNvbXBhw7HDrWEgY2RteJIBCmJvb2tfc3RvcmU",
            "phoneNumber": "5557047578",
            "email" : "", 
        },
        //{    REPETIDO
        //    "nombreEditorial": "FONDO DE CULTURA ECONOMICA",
        //    "direccion": "CARRETERA PICACHO AJUSCO Col. BOSQUES DEL PEDREGAL 14738 TLALPAN, CDMX",
        //    "linkMaps": "",
        //    "phoneNumber": "",
        //    "email" : "", 
        //},
        //{    NO UBICADO
        //    "nombreEditorial": "TERAN ROMAN GELACIO",
        //    "direccion": "INTERIOR M-ALLENDE LINEA 2 LOC-CM-09 Col. CENTRO AREA 9 06010 CUAUHTEMOC, CDMX",
        //    "linkMaps": "",
        //    "phoneNumber": "",
        //    "email" : "", 
        //},
        //{    REPETIDO
        //    "nombreEditorial": "FONDO DE CULTURA ECONOMICA",
        //    "direccion": "CARRETERA PICACHO AJUSCO Col. BOSQUES DEL PEDREGAL 14738 TLALPAN, CDMX",
        //    "linkMaps": "",
        //    "phoneNumber": "",
        //    "email" : "", 
        //},
        {   
            "nombreEditorial": "CARLOS JIMENEZ LOPEZ",
            "direccion": "MESONES LOC I-4 Col. CENTRO, AREA 9 06090 CUAUHTEMOC,MEXICO CDMX",
            "linkMaps": "https://www.google.com.mx/maps/place/Jimenez+Lopez+Carlos/@19.4281988,-99.1334859,19.5z/data=!4m5!3m4!1s0x85d1fecde8b08343:0x76eb06963363be0d!8m2!3d19.428312!4d-99.1334669",
            "phoneNumber": "",
            "email" : "", 
        },
        {
            "nombreEditorial": "SELENE JIMENEZ NICOLAS",
            "direccion": "JUSTO SIERRA SEP.304 Col. CENTRO 06020 CUAUHTEMOC, CDMX",
            "linkMaps": "https://www.google.com.mx/maps/place/Jimenez+Nicolas+Selene/@19.4352454,-99.1305047,19.5z/data=!4m5!3m4!1s0x85d1f933132697e1:0xf866cf2996fb6e8e!8m2!3d19.4353893!4d-99.1302689",
            "phoneNumber": "525555424095",
            "email" : "", 
        },
        //{   CERRRADO PERMANENTEMENTE
        //    "nombreEditorial": "TRISA DISTRIBUIDORES, S.A. DE C.V.",
        //    "direccion": "PRIVADA AGUSTIN GUTIERREZ Col. GENERAL PEDRO MARIA ANAYA 03340 BENITO JUAREZ CDMX",
        //    "linkMaps": "",
        //    "phoneNumber": "",
        //    "email" : "", 
        //},
    ]

    jQuery.each(distribuidoresArray, function(i, val) {
        if (val.phoneNumber != "" && val.email != "" && val.linkMaps != "" ) {
            var distribuidoresContainer = jQuery('<div class="item-card"></div>');
            var $element = distribuidoresContainer;
            $element.append(jQuery("<p class='editorial-name'>" + val.nombreEditorial + "</p>"));
            $element.append(jQuery("<p class='editorial-direction'><i class='fas fa-map-marker-alt'></i>" + val.direccion + "</p>"));
            $element.append(jQuery('<div class="c-distribuidores-cta" >'
                + '<a href=tel:' + val.phoneNumber + ' aria-label="Llamada directa" class="see-phone" rel="noopener noreferrer">'
                + '<i class="fas fa-phone-alt"></i></a>'
                + '<a href=mailto:' + val.email + ' aria-label="Enviar mail" class="see-mail" rel="noopener noreferrer">'
                + '<i class="fas fa-envelope"></i></a>'
                + '<a href=' + val.linkMaps + ' target="_blank" class="see-map" rel="noopener noreferrer">Ver Mapa'
                + '<i class="fas fa-external-link-alt"></i></a>'
                + '</div>'));
                distribuidoresContainer.append($element);
                jQuery(".c-distribuidores-items").append(distribuidoresContainer);
        }
        else if (val.phoneNumber == "" && val.email != "" && val.linkMaps != "" ) {
            var distribuidoresContainer = jQuery('<div class="item-card"></div>');
            var $element = distribuidoresContainer;
            $element.append(jQuery("<p class='editorial-name'>" + val.nombreEditorial + "</p>"));
            $element.append(jQuery("<p class='editorial-direction'><i class='fas fa-map-marker-alt'></i>" + val.direccion + "</p>"));
            $element.append(jQuery('<div class="c-distribuidores-cta" >'
                + '<a href=mailto:' + val.email + ' aria-label="Enviar mail" class="see-mail" rel="noopener noreferrer">'
                + '<i class="fas fa-envelope"></i></a>'
                + '<a href=' + val.linkMaps + ' target="_blank" class="see-map" rel="noopener noreferrer">Ver Mapa'
                + '<i class="fas fa-external-link-alt"></i></a>'
                + '</div>'));
                distribuidoresContainer.append($element);
                jQuery(".c-distribuidores-items").append(distribuidoresContainer);
        }
        else if (val.phoneNumber != "" && val.email == "" && val.linkMaps != "") {
            var distribuidoresContainer = jQuery('<div class="item-card"></div>');
            var $element = distribuidoresContainer;
            $element.append(jQuery("<p class='editorial-name'>" + val.nombreEditorial + "</p>"));
            $element.append(jQuery("<p class='editorial-direction'><i class='fas fa-map-marker-alt'></i>" + val.direccion + "</p>"));
            $element.append(jQuery('<div class="c-distribuidores-cta" >'
                + '<a href=tel:' + val.phoneNumber + ' aria-label="Llamada directa" class="see-phone" rel="noopener noreferrer">'
                + '<i class="fas fa-phone-alt"></i></a>'
                + '<a href=' + val.linkMaps + ' target="_blank" class="see-map" rel="noopener noreferrer">Ver Mapa'
                + '<i class="fas fa-external-link-alt"></i></a>'
                + '</div>'));
                distribuidoresContainer.append($element);
                jQuery(".c-distribuidores-items").append(distribuidoresContainer);
        }
        else if (val.phoneNumber == "" && val.email == "" && val.linkMaps != "") {
            var distribuidoresContainer = jQuery('<div class="item-card"></div>');
            var $element = distribuidoresContainer;
            $element.append(jQuery("<p class='editorial-name'>" + val.nombreEditorial + "</p>"));
            $element.append(jQuery("<p class='editorial-direction'><i class='fas fa-map-marker-alt'></i>" + val.direccion + "</p>"));
            $element.append(jQuery('<div class="c-distribuidores-cta" >'
                + '<a href=' + val.linkMaps + ' target="_blank" class="see-map" rel="noopener noreferrer">Ver Mapa'
                + '<i class="fas fa-external-link-alt"></i></a>'
                + '</div>'));
                distribuidoresContainer.append($element);
                jQuery(".c-distribuidores-items").append(distribuidoresContainer);
        }
    });
    </script>
<?php get_footer(); ?>
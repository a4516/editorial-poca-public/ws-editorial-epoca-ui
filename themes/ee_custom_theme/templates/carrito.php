<?php /* Template Name: Carrito de compras */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<?php get_header(); ?>
<main class="main-content carrito">
    <?php echo do_shortcode( '[woocommerce_cart]' ); ?>
</main>
<?php get_footer(); ?>